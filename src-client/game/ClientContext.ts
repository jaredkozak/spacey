import { Camera, Scene, WebGLRenderer } from 'three';
import { ClientEntity } from './clientEntity';

export interface GameContext {
    entities: ClientEntity[];
    drawContext: DrawContext,
    networkContext: NetworkContext,
}

export interface DrawContext {
    renderer: WebGLRenderer,
    camera: Camera,
    scene: Scene,
}

export interface NetworkContext {
    socket: SocketIOClient.Socket;
}