import { drawEntity, Entity, initEntity, networkUpdateEntity, updateEntity } from '../../src-common/engine/Entity';
import { DrawContext, GameContext, NetworkContext } from './ClientContext';

// common loop vars
let lastCounterUpdate = 0;
let gameContext: GameContext;

// frame vars
let drawContext: DrawContext;
let currentFrameID: number;
let lastDraw: number;
export let realFramesPerSecond = 0;

// update vars
let updating = false;
let lastUpdate: number;
let msPerUpdate = 1000 / 60;
let updateMakeup = 0;
let updatesInLastSecond = 0;
export let realUpdatesPerSecond = 0;

// network vars
let networking = false;
let networkContext: NetworkContext;
let lastNetworkUpdate: number;
let msPerNetworkUpdate = 1000 / 20;
let networkMakeup = 0;
let networkUpdatesInLastSecond = 0;
export let realNetworkUpdatesPerSecond = 0;


export function drawTick(delta: number, ctx: DrawContext) {
    for (const e of gameContext.entities) {
        drawEntity(e, delta, ctx);
    }
	ctx.renderer.render( ctx.scene, ctx.camera );
}

export function updateTick(delta: number, ctx: GameContext) {
    for (const e of gameContext.entities) {
        updateEntity(e, delta, ctx);
    }
}

export function networkTick(delta: number, ctx: NetworkContext) {

    for (const e of gameContext.entities) {
        networkUpdateEntity(e, delta, ctx);
    }
}

export function tickLoop() {
    // sanity checks on makeups
    if (updateMakeup > 1000) {
        updateMakeup = 0;
    }

    let now = performance.now();
    const deltaUpdate = now - lastUpdate;

    // reset counters
    if (now - lastCounterUpdate > 1000) {
        realUpdatesPerSecond = updatesInLastSecond;
        realNetworkUpdatesPerSecond = networkUpdatesInLastSecond;
        updatesInLastSecond = 0;
        networkUpdatesInLastSecond = 0;
        lastCounterUpdate = now;
    }

    // check if update tick
    if (updating && deltaUpdate + updateMakeup >= msPerUpdate) {
        updateMakeup += deltaUpdate - msPerUpdate
        lastUpdate = now;
        updatesInLastSecond++;
        // console.log(realUpdatesPerSecond);
        updateTick(deltaUpdate, gameContext);
    }

    now = performance.now();
    if (networkMakeup > 1000) {
        networkMakeup = 0;
    }
    const deltaNetworkUpdate = now - lastNetworkUpdate;

    // check if network update tick
    if (networking && deltaNetworkUpdate + networkMakeup >= msPerNetworkUpdate) {
        networkMakeup += deltaNetworkUpdate - msPerNetworkUpdate
        lastNetworkUpdate = now;
        networkUpdatesInLastSecond++;
        networkTick(deltaNetworkUpdate, networkContext);
    }

    setTimeout(tickLoop, 0);

}

export function drawLoop() {
    currentFrameID = requestAnimationFrame( drawLoop );
    const now = performance.now();
    const delta = now - lastDraw;
    lastDraw = now;

    drawTick(delta, drawContext);
}

export function initMainLoop(
    _drawContext: DrawContext,
    _networkContext: NetworkContext,
    _gameContext: GameContext,
) {

    drawContext = _drawContext;
    networkContext = _networkContext;
    gameContext = _gameContext;

    updating = true;
    networking = true;

    // init draw loop
	currentFrameID = requestAnimationFrame( () => {
        lastDraw = performance.now();
        currentFrameID = requestAnimationFrame( drawLoop );
    } );

    lastUpdate = performance.now();
    lastNetworkUpdate = lastUpdate;
    lastCounterUpdate = lastUpdate;
    tickLoop();
}
