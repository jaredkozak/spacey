import { Mesh } from 'three';
import { Entity } from '../../src-common/engine/Entity';
import { ClientComponent } from './clientComponent';


export interface ClientEntity extends Entity {
    components: ClientComponent[];
}
