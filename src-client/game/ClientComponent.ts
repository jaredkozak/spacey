import { Mesh, AnimationMixer } from 'three';
import { Component } from '../../src-common/engine/Component';
import { Entity } from '../../src-common/engine/Entity';
import { DrawContext, GameContext, NetworkContext } from './ClientContext';

export interface ClientComponent extends Component {

    init?(entity: Entity, component: Component, ctx?: GameContext);
    draw?(delta: number, entity: Entity, component: Component, ctx?: DrawContext);
    update?(delta: number, entity: Entity, component: Component, ctx?: GameContext);
    networkUpdate?(delta: number, entity: Entity, component: Component, ctx?: NetworkContext);
}

export interface MeshComponent extends ClientComponent {
    mesh?: Mesh;
    mixers?: AnimationMixer[];
}