import { initEntity } from '../src-common/engine/Entity';
import { dudeGenerator } from './entities/dude';
import { DrawContext, GameContext, NetworkContext } from './game/ClientContext';
import { ClientEntity } from './game/clientEntity';

export function addEntity(ctx: GameContext, ...entities: ClientEntity[]) {

    for (const e of entities) {
        initEntity(e, ctx);
    }
    ctx.entities.push(...entities);
}

export function initGame(drawContext: DrawContext, networkContext: NetworkContext): GameContext {

    const ctx: GameContext = {
        entities: [],
        drawContext,
        networkContext,
    }


    const dude = dudeGenerator();
    addEntity(ctx, dude);

    console.log(ctx);

    return ctx;
}
