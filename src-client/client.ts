import * as $ from 'jquery';
import * as client from 'socket.io-client';
import * as three from 'three';
import { PerspectiveCamera, Scene, WebGLRenderer } from 'three';
import { initGame } from './game';
import { DrawContext } from './game/ClientContext';
import { initMainLoop } from './game/mainLoop';
import './styles.css';

function initDevReload() {
    if (module.hot) {
        module.hot.addStatusHandler((status) => {
            if (status === 'ready') {
                console.log('Reloading page');
                location.reload();
            }
        });
    }
}

function initNet() {
    const socket = client();

    // $('button').click(() => {
    //     const msg = makeUMF({
    //         bdy: {
    //             jimmy: false,
    //         },
    //         to: 'action:game:accept'
    //     });
        
    //     socket.emit('msg', msg);
    // });

    return {
        socket,
    };
}

let renderer: WebGLRenderer;
let camera: PerspectiveCamera;
let scene: Scene;

function initCanvas(): DrawContext {
    //     canvas = document.querySelector("#gl");
    //     // Initialize the GL context
    //     gl = (canvas as any).getContext("webgl");
    //     gl.viewport(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight);

    //   if (gl === null) {
    //     alert("Unable to initialize WebGL. Your browser or machine may not support it.");
    //     return;
    //   }

    //   // Set clear color to black, fully opaque
    //   gl.clearColor(0.0, 0.0, 0.0, 1.0);
    //   // Clear the color buffer with specified clear color
    //   gl.clear(gl.COLOR_BUFFER_BIT);

    //   // TODO: wait for net to start drawing


	camera = new three.PerspectiveCamera( 70, window.innerWidth / window.innerHeight, 0.01, 5000 );

	scene = new three.Scene();

	// const geometry = new three.BoxGeometry( 0.2, 0.2, 0.2 );
	// const geometry = new three.ConeGeometry( 0.2, 0.2, 0.2 );
	// const material = new three.MeshNormalMaterial();

	// const mesh = new three.Mesh( geometry, material );
	// scene.add( mesh );

	renderer = new three.WebGLRenderer( {
        antialias: true,
        alpha: true,
    });


    renderer.gammaInput = true;
    renderer.gammaOutput = true;
    renderer.shadowMap.enabled = true;


	renderer.setSize( window.innerWidth, window.innerHeight );
    document.body.appendChild( renderer.domElement );

    return {
        renderer,
        camera,
        scene,
    }

}


function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );
}

// function animate() {
// 	requestAnimationFrame( animate );

// 	// mesh.rotation.x += 0.01;
// 	// mesh.rotation.y += 0.02;

// }

$(document).ready(() => {
    initDevReload();
    const drawContext = initCanvas();
    const networkContext = initNet();
    const gameContext = initGame(drawContext, networkContext);

    window.addEventListener( 'resize', onWindowResize, false );

    initMainLoop(
        drawContext,
        networkContext,
        gameContext,
    );

});
