import * as THREE from 'three';
import { OBJLoader } from 'three/examples/jsm/loaders/OBJLoader';
import { generateComponent } from '../../src-common/engine/Component';
import { Entity, generateEntity } from '../../src-common/engine/Entity';
import { MeshComponent, ClientComponent } from '../game/clientComponent';
import { GameContext, DrawContext } from '../game/ClientContext';
import { ClientEntity } from '../game/clientEntity';
import { Color, Mesh, ObjectLoader } from 'three';

// export function drawCircle(delta, e: ClientEntity, c: ClientComponent, ctx: DrawContext) {


// }

// export function init(e: ClientEntity, c: MeshComponent, ctx: GameContext) {
//     const geometry = new THREE.BoxGeometry( 1, 1, 1 );
//     const material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
    
//     c.mesh = new Mesh( geometry, material );

//     ctx.drawContext.camera.position.z = 5;

//     ctx.drawContext.scene.add( c.mesh );
// }


const mixers = [];
export function init(e: ClientEntity, c: MeshComponent, ctx: GameContext) {
    // const draw = ctx.drawContext;
    const { camera, scene } = ctx.drawContext;

    camera.position.set(0, 60, 1000);
    camera.rotation.set(-Math.PI / 8, 0, 0);
    // camera.rotateX(-Math.PI / 2);

    scene.background = new THREE.Color().setRGB(100,100,100);
    scene.fog = new THREE.Fog( scene.background.getHex(), 1, 5000 );

    // scene.background = new THREE.Color().setRGB(100,100,100);/
    // scene.background = new Color(100, 100, 100);
    // console.log(scene.background)


    const hemiLight = new THREE.HemisphereLight( 0xffffff, 0xffffff, 0.6 );
    hemiLight.color.setHSL( 0.6, 1, 0.6 );
    hemiLight.groundColor.setHSL( 0.095, 1, 0.75 );
    hemiLight.position.set( 0, 50, 0 );
    scene.add( hemiLight );

    const hemiLightHelper = new THREE.HemisphereLightHelper( hemiLight, 10 );
    scene.add( hemiLightHelper );


    const dirLight = new THREE.DirectionalLight( 0xffffff, 1 );
    dirLight.color.setHSL( 0.1, 1, 0.95 );
    dirLight.position.set( - 1, 1.75, 1 );
    dirLight.position.multiplyScalar( 30 );
    scene.add( dirLight );
    dirLight.castShadow = true;
    dirLight.shadow.mapSize.width = 2048;
    dirLight.shadow.mapSize.height = 2048;

    const  d = 50;
    dirLight.shadow.camera.left = - d;
    dirLight.shadow.camera.right = d;
    dirLight.shadow.camera.top = d;
    dirLight.shadow.camera.bottom = - d;
    dirLight.shadow.camera.far = 3500;
    dirLight.shadow.bias = - 0.0001;
    const dirLightHeper = new THREE.DirectionalLightHelper( dirLight, 10 );
    scene.add( dirLightHeper );


    // GROUND
    var groundGeo = new THREE.PlaneBufferGeometry( 10000, 10000 );
    var groundMat = new THREE.MeshLambertMaterial( { color: 0xffffff } );
    groundMat.color.setHSL( 0.095, 1, 0.75 );
    var ground = new THREE.Mesh( groundGeo, groundMat );
    ground.position.y = - 33;
    ground.rotation.x = - Math.PI / 2;
    ground.receiveShadow = true;
    scene.add( ground );


    // SKYDOME
    // var vertexShader = document.getElementById( 'vertexShader' ).textContent;
    // var fragmentShader = document.getElementById( 'fragmentShader' ).textContent;
    // var uniforms = {
    //     "topColor": { value: new THREE.Color( 0x0077ff ) },
    //     "bottomColor": { value: new THREE.Color( 0xffffff ) },
    //     "offset": { value: 33 },
    //     "exponent": { value: 0.6 }
    // };
    // uniforms[ "topColor" ].value.copy( hemiLight.color );
    // scene.fog.color.copy( uniforms[ "bottomColor" ].value );
    // var skyGeo = new THREE.SphereBufferGeometry( 4000, 32, 15 );
    // var skyMat = new THREE.ShaderMaterial( {
    //     uniforms: uniforms,
    //     vertexShader: vertexShader,
    //     fragmentShader: fragmentShader,
    //     side: THREE.BackSide
    // } );
    // var sky = new THREE.Mesh( skyGeo, skyMat );
    // scene.add( sky );

    // MODEL
    const loader = new OBJLoader();
    loader.load( 'assets/Minotaur.obj', function ( obj ) {
        
        const mesh = obj.children[0];
        c.mesh = mesh as Mesh;
        const s = 10;
        mesh.scale.set( s, s, s );
        mesh.position.y = 15;
        // mesh.rotation.y = - 1;
        mesh.castShadow = true;
        mesh.receiveShadow = true;
        scene.add( mesh );
        const mixer = new THREE.AnimationMixer( mesh );
        // mixer.clipAction( gltf.animations[ 0 ] ).setDuration( 1 ).play();
        c.mixers = [ mixer ];
    } );

}




export function update(delta, e: ClientEntity, c: MeshComponent, ctx: GameContext) {
    // const mesh = c.mesh as Mesh;
    // e.attributes.angle = e.attributes.angle || 0;

    // const anglesPerSecond = Math.PI / 6;
    // const angle = delta / 1000 * anglesPerSecond;
    // e.attributes.angle += angle;
    
    // console.log(angle);
    // mesh.rotateX(angle);
    // mesh.rotateY(angle);
    
    // mesh.position.x += 0.001 * delta;


    if (c.mesh) {
        // ctx.drawContext.camera.lookAt(c.mesh.position);
        c.mesh.position.z += 0.2 * delta;
    }
}

export function draw(delta, e: ClientEntity, c: MeshComponent, ctx: DrawContext) {
    if (c.mixers) {
        for (const m of c.mixers) {
            m.update(delta / 1000);
        }
    }
}


export function dudeGenerator() {

    const drawComponent = generateComponent('draw');
    drawComponent.draw = draw;
    drawComponent.init = init;
    drawComponent.update = update;
    

    const dude: Entity = generateEntity(
        'Dude',
        [
            drawComponent,
        ],
        {}
    );

    return dude;

}
