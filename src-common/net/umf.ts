import { v4 as uuid } from 'uuid';

/**
 * UMF Stands for Universal Messaging Format
 * More information can be found here: https://github.com/cjus/umf/blob/master/umf.md//
 */

export interface UMFMessage {

    /**
     * Message id
     *
     * Each message must contain an mid field with a universally unique identifier (UUID) as a value.
     */
    mid: string;

    /**
     * Refers to message id
     *
     * The Refers to Message ID (rmid) is a method of specifying that a message refers to another message.
     */
    rmi?: string;

    /**
     * The to field is used to specify message routing.
     * A message may be routed to other users, internal application handlers or other remote servers.
     */
    to: string;

    /**
     * forward
     *
     * The forward field can be used to designate where a message should be sent to.
     */
    fwd?: string;

    /**
     * from
     *
     * The from field is used to specify a source during message routing.
     * Like the to field, the value of a from field is a colon, or forward slash-separated list of sub names.
     */
    frm: string;

    /**
     * The message type field describes a message as being of a particular classification.
     */
    typ?: string;

    /**
     * Version
     *
     * UMF messages have a version field that identifies the version format for a given message.
     */
    ver: string;

    /**
     * Priority
     *
     * UMF documents may include an optional priority field.
     * If not present a default value equal to default priority is assumed.
     * If present, priority field values are in the range of 10 (highest) to 1 (lowest).
     */
    pri?: string;

    /**
     * timestamp
     *
     * UMF supports a timestamp field which indicates when a message was sent.
     * The format for a timestamp is ISO 8601, a standard date format. http://en.wikipedia.org/wiki/ISO_8601
     */
    ts: string;

    /**
     * Time to live
     *
     * The ttl field is used to specify how long a message may remain alive within a system.
     * The value of this field is an amount of time specified in seconds.
     */
    ttl?: string;

    /**
     * Headers
     *
     * When necessary communication protocol headers may be sent inside of a UMF message.
     */
    hdr?: { [ key: string ]: string; };

    /**
     * The body field is used to host an application-level custom object.
     * This is where an application may define a message content which
     * is meaningful in the context of its application.
     */
    bdy: any;

    /**
     * The signature field
     */
    sig?: string;
}

export type UMFMessageInput = Omit<UMFMessage, 'ts' | 'ver' | 'mid' | 'frm'>;
export function makeUMF(input: UMFMessageInput, ctx?): UMFMessage {
    const me = 'uid:54';

    return {
        ...input,
        ts: new Date().toISOString(),
        mid: uuid(),
        ver: 'UMF/1.4',
        frm: me,
    };
}
