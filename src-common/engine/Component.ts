import { v4 as uuid } from 'uuid';
import { Entity } from './Entity';

export interface Component {
    id: string;
    name: string;

    init?(entity: Entity, component: Component, ctx?: any);
    draw?(delta: number, entity: Entity, component: Component, ctx?: any);
    update?(delta: number, entity: Entity, component: Component, ctx?: any);
    networkUpdate?(delta: number, entity: Entity, component: Component, ctx?: any);
}

export function generateComponent(
    name = 'Unnamed Component',
): Component {

    return {
        id: uuid(),
        name,
    };
}
