import { v4 as uuid } from 'uuid';
import { Component } from './Component';
import { Vector3 } from 'three';

export interface Attributes {
    [ key: string ]: any;
}

export interface Entity {
    id: string;
    name: string;

    pos: Vector3;

    components: Component[];
    attributes: Attributes;
}

export function generateEntity(
    name = 'Unnamed Entity',
    components: Component[] = [],
    attributes: Attributes = {},
    pos = new Vector3(0, 0, 0),
): Entity {
    return {
        id: uuid(),
        attributes,
        components,
        name,
        pos,
    };
}

export function updateEntity(entity: Entity, delta: number, ctx?: any) {
    for (const component of entity.components) {
        if (component.hasOwnProperty('update')) {
            component.update(delta, entity, component, ctx);
        }
    }
}

export function initEntity(entity: Entity, ctx?: any) {
    for (const component of entity.components) {
        if (component.hasOwnProperty('init')) {
            component.init(entity, component, ctx);
        }
    }
}

export function drawEntity(entity: Entity, delta?: number, ctx?: any) {
    for (const component of entity.components) {
        if (component.hasOwnProperty('draw')) {
            component.draw(delta, entity, component, ctx);
        }
    }
}

export function networkUpdateEntity(entity: Entity, delta?: number, ctx?: any) {
    for (const component of entity.components) {
        if (component.hasOwnProperty('networkUpdate')) {
            component.networkUpdate(delta, entity, component, ctx);
        }
    }
}
