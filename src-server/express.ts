// import * as express from 'express';
// tslint:disable-next-line: no-var-requires
import * as express from 'express';

export function initExpress() {
    const app = express();

    app.use('/assets', express.static('assets', {
        cacheControl: false,
        // maxAge:
    }));

    return app;
}
