import * as HtmlWebpackPlugin from 'html-webpack-plugin';
import * as path from 'path';
import { Configuration, HotModuleReplacementPlugin } from 'webpack';

const template = path.join(__dirname, '../../src-client/index.html');

export const config: Configuration = {
  devtool: 'inline-source-map',
  entry: {
    client: [ path.join(__dirname, '../../src-client/client.ts'), 'webpack-hot-middleware/client' ],
  },
  mode: 'development',
  module: {
    rules: [
      {
        exclude: /node_modules/,
        test: /\.tsx?$/,
        use: 'ts-loader',
      },
      {
        test: /\.css$/,
        exclude: /node_modules/,
        use: [
          'style-loader', 'css-loader',
        ],
      },
    ],
  },
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, '../../dist/client'),
    publicPath: '/',
  },
  plugins: [
    new HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
      title: 'Spacey',
      template,
      inject: true,
      meta: {
        viewport: 'width=device-width, initial-scale=1, shrink-to-fit=no',
      },
    }),
  ],
  resolve: {
    extensions: [ '.tsx', '.ts', '.js', 'scss', 'css' ],
  },
};
