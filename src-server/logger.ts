import * as winston from 'winston';

let date = new Date().toISOString();
const logFormat = winston.format.printf(function(info) {
  return `${date}-${info.level}: ${JSON.stringify(info.message, null, 2)}\n`;
});

export const logger = winston.createLogger({
    level: 'info',
    transports: [
        new winston.transports.Console({
            format: winston.format.combine(winston.format.colorize(), logFormat)
        }),
    // new winston.transports.File({ filename: 'combined.log' }),
    ],
});
