import * as debug from 'debug';
import { Socket } from 'socket.io';
import { accept } from './actions/game/accept';
import { logger } from './logger';

export function handleMessage(socket, payload) {
    // console.log('911111');
    // whatssss
    // console.log(socket);
    logger.info(payload);
    debug('pewpew');

    accept(undefined);
}

export function handleConnection(socket: Socket) {

    logger.info('new connection');
    socket.compress(true);
}
