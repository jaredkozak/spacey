
import * as webpack from 'webpack';
import * as webpackDevMiddleware from 'webpack-dev-middleware';
import * as webpackHotMiddleware from 'webpack-hot-middleware';
import { config as webpackConfig } from './webpack.client.config';

let compiler: webpack.Compiler;

export function serveClientUse(app, data: ReturnType<typeof serveClient>) {

    app.use(data.devMiddleware);
    app.use(data.hotMiddleware);
}

export function serveClient(app) {

    compiler = webpack(webpackConfig);
    const devMiddleware = webpackDevMiddleware(compiler, {
        publicPath: webpackConfig.output.publicPath,
        // writeToDisk: true,
        watchOptions: {
        poll: true,
        },
        stats: 'minimal',
    });

    const hotMiddleware = webpackHotMiddleware(compiler, {});

    const d = {
        compiler,
        devMiddleware,
        hotMiddleware,
    };

    serveClientUse(app, d);

    return d;
}
