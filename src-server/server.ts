import { init, start } from './init';

function initHotReload() {

  // if (module.hot.data && module.hot.data.init) {
  //   d = module.hot.data.init;
  //   console.log(d);
  // }

  // reload current file
  module.hot.decline();

  // module.hot.dispose(async (data) => {
  //   console.log('dooper');
  //   await stop();
  //   data.init = d;
  // });

  // reload sockets
  module.hot.accept('./init.ts', async () => {
    init();
    start();
    // app = module.hot.data.app;//
    // server = createServer();
    // sockets = module.hot.data.sockets;
    // sockets.attach(server);
    // serveClient(app);
    // listen();

  });

  // reload express
  // module.hot.accept('./express.ts', () => {
  //   // initExpress();
  //   // serveClient(app);
  // });

  // module.hot.accept([ './webpack.client.config.ts' ]);

  // module.hot.decline([ './client-server.ts' ]);
}

if (module.hot) {
  initHotReload();
}

init();
start();
