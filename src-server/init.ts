import { createServer, Server } from 'http';
import { Server as SocketServer } from 'socket.io';
import { serveClient, serveClientUse } from './client-server';
import { initExpress } from './express';
import { logger } from './logger';
import { initSockets } from './sockets';

export let app;
export let server: Server;
export let sockets: SocketServer;
export let webpacks: ReturnType<typeof serveClient>;

export function init() {

  app = initExpress();

  server = createServer(app);

  sockets = initSockets(server);

  if (module.hot && module.hot.data && module.hot.data.webpacks) {
    webpacks = module.hot.data.webpacks;
    serveClientUse(app, webpacks);
  } else {
    webpacks = serveClient(app);
  }

  if (module.hot) {

    module.hot.dispose(async (data) => {
      data.webpacks = webpacks;
      await stop();
    });
  }

}

export async function stop() {

  return new Promise((resolve) => {
    if (server) {
      server.close(() => {
        resolve();
      });
    } else {
      resolve();
    }
  });
}

export async function start() {
  if (server.listening) {
    return;
  }

  server.listen(3000, () => {
    logger.info('listening on http://127.0.0.1:3000');
  });
}
