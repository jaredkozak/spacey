import { accept } from './actions/game/accept';

export const actions = {
    game: {
        accept,
    },
};
