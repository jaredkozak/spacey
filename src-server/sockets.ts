import { Server } from 'http';
import * as sockets from 'socket.io';
import * as handler from './handler';

export let io: sockets.Server;
export let ns: sockets.Namespace;

export function setIO(newIO: sockets.Server) {
    io = newIO;
    ns = io.of('/');
}

// try not to change anything here
export function initSockets(server: Server) {

    io = sockets(server);
    ns = io.of('/');
    initHandler();

    return io;

}

export function initHandler() {
    ns.removeAllListeners();

    ns.on('connection', (socket) => {
        handler.handleConnection(socket);

        socket.on('msg', (msg) => {
            handler.handleMessage(socket, msg);
        });
    });
}

if (module.hot) {

    module.hot.accept('./handler', () => {
        initHandler();
    });

    module.hot.dispose((data) => {
        data.io = io;
    });
}
